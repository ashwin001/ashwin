//Google map initialize start By Ashwin Bhayal :24/10/2016
var map,autocomplete;
var componentForm = {
	street_number: 'short_name',
	route: 'long_name',
	locality: 'long_name',
	administrative_area_level_1: 'short_name',
	country: 'long_name',
	postal_code: 'short_name'
};
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
	  center: {lat: -34.397, lng: 150.644},
	  zoom: 8
	});
	autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')),{types: ['geocode']});
	getlistofaddress();
}
//Google map initialize end By Ashwin Bhayal :24/10/2016
//Get List of address Start by Ashwin Bhayal : 24/10/2016
function getlistofaddress(){
	$.ajax({url: "data.php?getaddresslist=getaddresslist", success: function(result){		
		var resultdata=jQuery.parseJSON(result);
		var listofaddresshtml="";
		for(var i=0;i<resultdata.length;i++){
			listofaddresshtml+="<h3>"+resultdata[i].address+"</h3><div><b>Latitude : </b>"+resultdata[i].latitude+"<br/><b>Longitude : </b>"+resultdata[i].longitude+"</div>"
		}
        $("#accordion").html(listofaddresshtml);
    }});
}
//Get List of address end by Ashwin Bhayal : 24/10/2016
//Add address Start by Ashwin Bhayal : 24/10/2016
function getlatlng(){
	var place = autocomplete.getPlace();
	var latlng={lat: place.geometry.location.lat(), lng: place.geometry.location.lng()}
	var contentString = '<div id="content">'+document.getElementById('autocomplete').value+'</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: latlng,
          map: map,
          title: 'Address'
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
		map.setCenter(new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng()));
	$.ajax({url: "data.php?searchaddress=searchaddress&address="+document.getElementById('autocomplete').value+"&Lat="+place.geometry.location.lat()+"&Lng="+place.geometry.location.lng(), success: function(result){
        getlistofaddress();
    }});
}
//Add address End by Ashwin Bhayal : 24/10/2016
$( function() {
	$( "#accordion" ).accordion();
} );
